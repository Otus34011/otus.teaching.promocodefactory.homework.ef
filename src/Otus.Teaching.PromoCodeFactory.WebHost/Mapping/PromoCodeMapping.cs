﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapping
{
    public interface IPromoCodeMapping
    {
        PromoCode CreateMap(GivePromoCodeRequest request, Preference preference, Customer customer, Employee employee);
    }

    public class PromoCodeMapping : IPromoCodeMapping
    {
        public PromoCode CreateMap(GivePromoCodeRequest request, Preference preference, Customer customer, Employee employee)
        {
            return new PromoCode
            {
                Id = Guid.NewGuid(),

                PartnerName = request.PartnerName,
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,

                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddMonths(1),

                PartnerManagerId = employee.Id,
                PreferenceId = preference.Id,
                CustomerId = customer.Id
            };
        }
    }
}
