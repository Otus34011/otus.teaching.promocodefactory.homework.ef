﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapping
{
    public interface ICustomerMapping
    {
        Customer CreateMap(CreateOrEditCustomerRequest request, IEnumerable<Preference> preferences, Customer customer);
    }

    public class CustomerMapping : ICustomerMapping
    {
        public Customer CreateMap(CreateOrEditCustomerRequest request, IEnumerable<Preference> preferences, Customer customer)
        {
            customer.Email = request.Email;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                Customer = customer,
                Preference = x
            }).ToList();

            return customer;
        }
    }
}
