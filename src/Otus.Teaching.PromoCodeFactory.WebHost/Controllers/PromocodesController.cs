﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mapping;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IPromoCodeMapping _promoCodeMapping;

        public PromocodesController(
            IRepository<PromoCode> promoCodesRepository,
            IRepository<Preference> preferencesRepository, 
            IRepository<Customer> customersRepository,
            IRepository<Employee> employeeRepository,
            IPromoCodeMapping promoCodeMapping)
        {
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
            _employeeRepository = employeeRepository;
            _promoCodeMapping = promoCodeMapping;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = await _promoCodesRepository.GetAllAsync();

            var response = promoCodes.Select(x => new PromoCodeShortResponse()
            {
                Id = x.Id,
                Code = x.Code,
                BeginDate = x.BeginDate.ToShortDateString(),
                EndDate = x.EndDate.ToShortDateString(),
                PartnerName = x.PartnerName,
                ServiceInfo = x.ServiceInfo
            }).ToList();

            return Ok(response);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиенту с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preferences = await _preferencesRepository.GetAllAsync();
            var customers = await _customersRepository.GetAllAsync();
            var employees = await _employeeRepository.GetAllAsync();

            var preference = preferences.FirstOrDefault(x => x.Name == request.Preference);
            var customer = customers.FirstOrDefault(x => x.Preferences.Any(y => y.PreferenceId == preference.Id));
            var employee = employees.FirstOrDefault(x => x.Role.Name == "PartnerManager");

            if(preference == null || customer == null || employee == null)
                return BadRequest("Невозможно выдать промокод");

            var promocode = _promoCodeMapping.CreateMap(request, preference, customer, employee);
          
            await _promoCodesRepository.AddAsync(promocode);
            return Ok();
        }
    }
}