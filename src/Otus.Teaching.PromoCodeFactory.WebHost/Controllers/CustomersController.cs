﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mapping;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly ICustomerMapping _customerMapping;

        public CustomersController(
            IRepository<Customer> customerRepository, 
            IRepository<Preference> preferenceRepository, 
            IRepository<PromoCode> promocodeRepository, 
            ICustomerMapping customerMapping)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _promocodeRepository = promocodeRepository;
            _customerMapping = customerMapping;
        }

        /// <summary>
        /// Получить список клиентов
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers
                .Select(x => new CustomerShortResponse() 
                { 
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email
                }).ToList();

            return Ok(response);
        }

        /// <summary>
        /// Получить клиента вместе промокодами
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            var response = new CustomerResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Preferences = customer.Preferences.Select(x => new PreferenceResponse()
                {
                    Id = x.PreferenceId,
                    Name = x.Preference.Name
                }).ToList(),
                PromoCodes = customer.PromoCodes.Select(x => new PromoCodeShortResponse()
                {
                    Id = x.Id,
                    Code = x.Code,
                    BeginDate = x.BeginDate.ToLongDateString(),
                    EndDate = x.EndDate.ToLongDateString(),
                    ServiceInfo = x.ServiceInfo,
                    PartnerName = x.PartnerName
                }).ToList()
            };

            return Ok(response);
        }


        /// <summary>
        /// Добавить клиента вместе с предпочтениями
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            if (request == null)
                return BadRequest("Отсутствуют данные для заполнения");

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            var customer = _customerMapping.CreateMap(request, preferences, new Customer());

            await _customerRepository.AddAsync(customer);

            var response = new CustomerShortResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email
            };

            return Ok(response);
        }

        /// <summary>
        /// Обновить данные клиента вместе с предпочтениями
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            if (request == null)
                return BadRequest("Отсутствуют данные для заполнения");

            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound($"Customer {id} doesn't found");

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            _customerMapping.CreateMap(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return Ok();
        }

        /// <summary>
        /// Удалить клиента вместе с промокодами
        /// </summary>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null) return NotFound($"Customer {id} doesn't found");

            foreach(var promo in customer.PromoCodes.ToList())
            {
                var promocode = await _promocodeRepository.GetByIdAsync(promo.Id);
                if (promocode == null) continue;

                await _promocodeRepository.DeleteAsync(promocode);
            }

            await _customerRepository.DeleteAsync(customer);

            return Ok();
        }
    }
}