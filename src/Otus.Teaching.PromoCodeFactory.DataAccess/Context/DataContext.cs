﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Context
{
    public class DataContext : DbContext
    {
        private const int Len = 100;

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Role> Roles { get; set; }     

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();

            AddRange(FakeDataFactory.Employees);
            AddRange(FakeDataFactory.Preferences);
            AddRange(FakeDataFactory.Customers);
            AddRange(FakeDataFactory.PromoCodes);
            SaveChanges();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerPreference>().HasKey(cp => new { cp.CustomerId, cp.PreferenceId });
            modelBuilder.Entity<CustomerPreference>().HasOne(cp => cp.Customer).WithMany(cp => cp.Preferences).HasForeignKey(cp => cp.CustomerId);
            modelBuilder.Entity<CustomerPreference>().HasOne(cp => cp.Preference).WithMany().HasForeignKey(cp => cp.PreferenceId);

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.Property(x => x.FirstName).HasMaxLength(Len);
                entity.Property(x => x.LastName).HasMaxLength(Len);
                entity.Property(x => x.Email).HasMaxLength(Len);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(x => x.Name).HasMaxLength(Len);
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.Property(x => x.FirstName).HasMaxLength(Len);
                entity.Property(x => x.LastName).HasMaxLength(Len);
                entity.Property(x => x.Email).HasMaxLength(Len);
            });

            modelBuilder.Entity<Preference>(entity =>
            {
                entity.Property(x => x.Name).HasMaxLength(Len);
            });

            modelBuilder.Entity<PromoCode>(entity =>
            {
                entity.Property(x => x.Code).HasMaxLength(Len);
                entity.Property(x => x.ServiceInfo).HasMaxLength(Len);
                entity.Property(x => x.PartnerName).HasMaxLength(Len);
                entity.Property(x => x.Code).HasMaxLength(Len);
            });
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            
        }
    }
}
